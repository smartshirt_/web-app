from sqlalchemy import Column, Integer, String
from db import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    email = Column(String(120), unique=True)

    def __init__(self, name=None, email=None):
        self.name = name
        self.email = email

    def __repr__(self):
        return '<User %r>' % (self.name)

class Alert(Base):
    __tablename__ = "alerts"
    id = Column(Integer, primary_key=True)
    coords = Column(String(50), unique=False)
    bpm = Column(String(120), unique=False)
    temperature = Column(String(120), unique=False)

    def __init__(self,coords = None, bpm = None, temperature = None):
        self.coords = coords;
        self.bpm = bpm;
        self.temperature = temperature;

    def __repr__(self):
        return "<Alert" + str(self.id) + ">"

