
# A very simple Flask Hello World app for you to get started with...

from flask import Flask,render_template,request
from db import db_session
from models import User, Alert
import json



app = Flask(__name__)

@app.route('/alertmap/', methods=["GET"])
def hello_world():
    if request.method == "POST":
        pass
    else:
        return render_template("map.html");



@app.route("/status/", methods=["GET", "POST"])
def status():
    if request.method == "GET":
        if (Alert.query.first() is not None):
            a = Alert.query.order_by("id desc").first()
            return json.dumps({
                "status" : "ok",
                "coords" : a.coords,
                "bpm": a.bpm,
                "temperature" : a.temperature
                })
        else:
            return json.dumps(
                {"status" : "no"}
                )
    else:

        bpm = request.form["bpm"];
        coords = request.form["coords"];
        temperature = request.form["temperature"];


        a = Alert(coords = coords, bpm = bpm, temperature = temperature)

        db_session.add(a)
        db_session.commit()
        return "ok";

@app.route("/delete/", methods=["GET"])
def delete():
    a = Alert.query.first()
    db_session.delete(a)
    db_session.commit()
    return "ok"



@app.teardown_appcontext
def close_connection(exception):
    db_session.remove()


